<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"
	import="java.io.*,java.util.*, javax.servlet.*, java.text.SimpleDateFormat"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="formHandler" class="beans.FormBean" scope="request" />

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Success! Order Confirmation</title>
<%
	Random rand = new Random();
	int n = rand.nextInt(90000) + 10000;
	System.out.println(n);
%>
<%
	SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
	Calendar c = Calendar.getInstance();
	c.setTime(new Date()); // Now use today date.
	c.add(Calendar.DATE, 5); // Adding 5 days
	String output = sdf.format(c.getTime());
%>
</head>
<body background=images/wallpaper.jpg
	style="color: rgba(134, 26, 42, 0.75);">
	Thank you for your order.
	<br> Your confirmation number is:
	<b><%=n%></b>
	<br> Your estimated delivery date is:
	<b><%=output%></b>
	<!-- 5 days after the date of order -->
</body>
</html>