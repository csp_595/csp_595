<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="css/style.css">
<script type="text/javascript"
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
<script src="js/jquery.autocomplete.js"></script>
</head>
<body background=images/wallpaper.jpg onLoad="login()">
<%
            String message = "";
                message = "Welcome back user! Use promo code WBK123 to receive discount on your next shoe purchase!";
        %>
	<div id="north">
		<div class="header">
			<ul class="header_ul">

				<li><form action="search.jsp" method="POST">
						<input type="text" id="search" name="search" /> <input
							type="submit" value=""
							style="background: url(images/search.png) no-repeat" />
					</form></li>
			</ul>
			<ul class="header_ul_right">
				<li><a href="index.html" title="Login"><img
						src="images/login.png"></a></li>
				<li><a href="index.html" title="New User"><img
						src="images/new.png"></a></li>
			</ul>
		</div>
	</div>
	<div id="west">
		<div class=navigation>
			<ul class=nav_ul>
				<li><a href="index.jsp"><img src="images/logo.png"></a></li>
				<li><a href="#openWomen">WOMEN</a></li>
				<li><a href="#openMen">MEN</a></li>
				<li><a href="#openKids">KIDS</a></li>
			</ul>
		</div>
	</div>
	<div id="center">
		<div class=maincarousel>
			<div class=carousel>
				<ul class=panes>
					<li>
						<h2>OH PAIR!</h2> <img
						src=http://www.donbleek.com/wp-content/uploads/2014/09/Jimmy-Choo-Opens-Flagship-in-London-1-500x333.jpg
						alt="">
					</li>

					<li>
						<h2>HEEL2TOE</h2> <img
						src=http://blog.appycouple.com/wp-content/uploads/2012/04/Red-Wedding-Shoes2.jpg
						alt="">
					</li>

					<li>
						<h2>FOOT WORKS</h2> <img
						src=http://www.frenchtruckers.com/wp-content/uploads/2011/08/Band-of-Outsiders-semi-brogue-Oxford-shoes-sole-500x333.jpg
						alt="">
					</li>

					<li>
						<h2>TWINKLE LITTLE TOES</h2> <img
						src=http://www.noordinaryhomestead.com/wp-content/uploads/2011/06/newsneaks-500x333.jpg
						alt="">
					</li>

					<li>
						<h2>REBOOTED</h2> <img
						src=http://s6.favim.com/orig/65/boots-floral-flowers-hipster-Favim.com-600160.jpg
						alt="">
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div id="south">
		<div class=footer>
			<a href="">Contact</a> | <a href="">About Us</a> | <a href="">FAQs</a>
		</div>
	</div>
	<div id="openWomen" class="modalDialog">
		<div>
			<a href="#close" title="Close" class="close">X</a>
			<h2>Stiletto</h2>
			<p>
				<img src="images/woman-boot.png">This is a boot. Women wear
				it. And then they trip when its on their foot! Because this boot is
				meant to make your foot look pretty and to make you look tall. Not
				exactly to keep your balance! HA..!
			</p>
			<p>Size</p>
			<p>
			<p />
		</div>
	</div>
	<div id="openMen" class="modalDialog">
		<div>
			<a href="#close" title="Close" class="close">X</a>
			<h2>Boot</h2>
			<p>
				<img src="images/man-boot.png">This is a boot. Men wear it. It
				is comfy! HA..!
			</p>
			<p>Size</p>
			<p>
			<p />
		</div>
	</div>
	<div id="openKids" class="modalDialog">
		<div>
			<a href="#close" title="Close" class="close">X</a>
			<h2>Shoe</h2>
			<p>
				<img src="images/kid-shoe.png">This is a shoe. Kids wear it.
				It keeps their twinkle toes nice and snug! HA..!
			</p>
			<p>Size</p>
			<p>
			<p />
		</div>
	</div>
	    <script type="text/javascript">
         function login(){   alert('<%=message%>');}
        </script>
	<script>
		$("#search").autocomplete("getdata.jsp");
	</script>
</body>
</html>
