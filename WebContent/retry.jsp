<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="formHandler" class="beans.FormBean" scope="request" />
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Checkout</title>
</head>
<body background=images/wallpaper.jpg
	style="color: rgba(134, 26, 42, 0.75);">
	<table border=1 align="left">
		<tr style="background-color: #555555; color: #ffffff;">
			<td>New User? Sign Up</td>
			<td>Returning User? Sign In</td>
		</tr>
		<tr>
			<td>
				<form method="post" action="register.jsp">
					<br> First Name:<input type="text" name="first" value='<%=formHandler.getfirst()%>' /> 
					<br><font size=2 color=red><%=formHandler.getErrorMsg("first")%></font>
					<br> Last Name: <input type="text" name="last" value='<%=formHandler.getlast()%>' /> 
					<br> <font size=2 color=red><%=formHandler.getErrorMsg("last")%></font>
					<br> Address: <textarea name="address" rows="6" cols="30" value='<%=formHandler.getaddress()%>'></textarea>
					<br> <font size=2 color=red><%=formHandler.getErrorMsg("address")%></font>
					<br> Phone Number:<input type="text" name="phone_number" />
					<br> Email:<input type="text" name="email" value='<%=formHandler.getEmail()%>' />
					<br> <font size=2 color=red><%=formHandler.getErrorMsg("email")%></font>
					<br> Username:<input type="text" name="uname" value='<%=formHandler.getuname()%>' /> 
					<br> <font size=2 color=red><%=formHandler.getErrorMsg("uname")%></font>
					<br> Password:<input type="text" name="password" value='<%=formHandler.getpassword()%>' /> 
					<br> <font size=2 color=red><%=formHandler.getErrorMsg("password")%></font>
					<br> Confirm Password:<input type="text" name="c_password" value='<%=formHandler.getc_password()%>' /> 
					<br> <font size=2 color=red><%=formHandler.getErrorMsg("c_password")%></font>
					<br> <b>Credit Card Type:</b> 
					<br> 
						<input type="checkbox" name="c_type" value="visa" <%=formHandler.isCbSelected("Visa")%>>Visa
						<input type="checkbox" name="c_type" value="mastercard" <%=formHandler.isCbSelected("mastercard")%>>Mastercard 
						<input type="checkbox" name="c_type" value="ae" <%=formHandler.isCbSelected("ae")%>>American Express 
						<input type="checkbox" name="c_type" value="discover" <%=formHandler.isCbSelected("discover")%>>Discover 
					<br> Credit Card Number:<input type="text" name="card_number" />
					<br>Expiration Date:<input type="text" name="exp_date" /> 
					<br> CVV Number:<input type="text" name="cvv" />
					<br> <b>Would you like to receive notifications on our special sales?</b> 
						<input type="radio" name="notify" value="Yes" <%=formHandler.isRbSelected("Yes")%>>Yes 
						<input type="radio" name="notify" value="No" <%=formHandler.isRbSelected("No")%>> No 
					<br> 
					<br> <input type="submit" value="Confirm" /> <input type="reset" value="Reset" />
				</form>
			</td>
			<td>
				<form method="post" action="">
					<br> Username:<input type="text" name="l_uname" />
					<br> Password:<input type="text" name="l_password" /> 
					<br> <input type="submit" value="Confirm" /> <input type="reset" value="Reset" />
				</form>
			</td>
		</tr>
	</table>
</body>
</html>