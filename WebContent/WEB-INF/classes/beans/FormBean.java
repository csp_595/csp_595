package beans;

import java.util.*;
public class FormBean {
  private String first;
  private String last;
  private String email;
  private String uname;
  private String password;
  private String c_password;
  private String address;
  private String[] c_type;
  private String notify;
  private Hashtable errors;
  public boolean validate() {
    boolean allOk=true;
    if (first.equals("")) {
      errors.put("first","Please enter your first name");
      first="";
      allOk=false;
    }
    if (last.equals("")) {
      errors.put("last","Please enter your last name");
      last="";
      allOk=false;
    }
    if (email.equals("") || (email.indexOf('@') == -1)) {
      errors.put("email","Please enter a valid email address");
      email="";
      allOk=false;
    }
    if (uname.equals("")) {
      errors.put("uname","Please enter a uname");
      uname="";
      allOk=false;
    }
    if (password.equals("") ) {
      errors.put("password","Please enter a valid password");
      password="";
      allOk=false;
    }
    if (!password.equals("") && (c_password.equals("") || 
        !password.equals(c_password))) {
      errors.put("c_password","Please confirm your password");
      c_password="";
      allOk=false;
    }
    if (address.equals("")) {
      errors.put("address","Please enter a valid address");
      address="";
      allOk=false;
    }
    return allOk;
  }
  public String getErrorMsg(String s) {
    String errorMsg =(String)errors.get(s.trim());
    return (errorMsg == null) ? "":errorMsg;
  }
  public FormBean() {
    first="";
    last="";
    email="";
    uname="";
    password="";
    c_password="";
    address="";
    c_type = new String[] { "1" };
    notify="";
    errors = new Hashtable();
  }
  public String getfirst() {
    return first;
  }
  public String getlast() {
    return last;
  }
  public String getEmail() {
    return email;
  }
  public String getuname() {
    return uname;
  }
  public String getpassword() {
    return password;
  }
  public String getc_password() {
    return c_password;
  }
  public String getaddress() {
    return address;
  }
  public String getNotify() {
    return notify;
  }
  public String[] getc_type() {
    return c_type;
  }
  public String isCbSelected(String s) {
    boolean found=false;
    if (!c_type[0].equals("1")) {
      for (int i = 0; i < c_type.length; i++) {
        if (c_type[i].equals(s)) {
          found=true;  
          break;
        }
      }
      if (found) return "checked";
    } 
    return "";
  }
  public String isRbSelected(String s) {
    return (notify.equals(s))? "checked" : "";
  }
  public void setfirst(String fname) {
    first =fname;
  }
  public void setlast(String lname) {
    last =lname;
  }
  public void setEmail(String eml) {
    email=eml;
  }
  public void setuname(String u) {
    uname=u;
  }
  public void  setpassword(String p1) {
    password=p1;
  }
  public void  setc_password(String p2) {
    c_password=p2;
  }
  public void setaddress(String z) {
    address=z;
  }
  public void setc_type(String[] card_type) {
    c_type=card_type;
  }
  public void setErrors(String key, String msg) {
    errors.put(key,msg);
  }
  public void setNotify(String n) {
    notify=n;
  }
}