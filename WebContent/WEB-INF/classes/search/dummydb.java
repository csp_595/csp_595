package search;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
public class dummydb {
	 private int totalSearchText;
	    private String data = "Stiletto, Boot, Shoe";
	    private List<String> SearchText;
	    public dummydb() {
	        SearchText = new ArrayList<String>();
	        StringTokenizer st = new StringTokenizer(data, ",");
	         
	        while(st.hasMoreTokens()) {
	            SearchText.add(st.nextToken().trim());
	        }
	        totalSearchText = SearchText.size();
	    }
	     
	    public List<String> getData(String query) {
	        String search = null;
	        query = query.toLowerCase();
	        List<String> matched = new ArrayList<String>();
	        for(int i=0; i<totalSearchText; i++) {
	            search = SearchText.get(i).toLowerCase();
	            if(search.startsWith(query)) {
	                matched.add(SearchText.get(i));
	            }
	        }
	        return matched;
	    }
	}