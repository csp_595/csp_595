<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.*"%>
<%! 
    ResourceBundle bundle =null;
    public void jspInit() {
       bundle = ResourceBundle.getBundle("forms");
      }
%>
<jsp:useBean id="formHandler" class="beans.FormBean" scope="request">
	
	<jsp:setProperty name="formHandler" property="*" />

</jsp:useBean>

<% if (formHandler.validate()) { %>
<jsp:forward page="<%=bundle.getString(\"process.success\")%>" />
<% }  else { %>
<jsp:forward page="<%=bundle.getString(\"process.retry\")%>" />
<% } %>